/*
 * AliGenPair.h
 *
 *  Created on: Feb 3, 2016
 *      Author: markus
 */

#ifndef ALIGENPAIR_H_
#define ALIGENPAIR_H_

#include "AliGenerator.h"

class TVector3;

class AliGenPair : public AliGenerator {
public:
	AliGenPair();
	virtual ~AliGenPair();

	virtual void Init();
	virtual void Generate();

	void SetFractionSubleadingPt(Double_t fractionMin, Double_t fractionMax = 1.) { fFractionSubleadingPt[0] = fractionMin; fFractionSubleadingPt[1] =  fractionMax; }
	void SetEtaRange(Double_t etamin, Double_t etamax) {fEtaRange[0] = etamin; fEtaRange[1] = etamax; }
	void SetDeltaRRange(Double_t deltaRMin, Double_t deltaRMax) { fDeltaR[0] = deltaRMin; fDeltaR[1] = deltaRMax; fMode = kRadius; }
	void SetDeltaEtaRange(Double_t detamin, Double_t detamax) { fDeltaEtaRange[0] = detamin; fDeltaEtaRange[1] = detamax; fMode = kEtaPhi; }
	void SetDeltaPhiRange(Double_t dphimin, Double_t dphimax) { fDeltaPhiRange[0] = dphimin; fDeltaPhiRange[1] = dphimax; fMode = kEtaPhi; }
	void SetPdgCode(Int_t pdgcode) { fPdgCodeLeading = fPdgCodeSubleading = pdgcode; }
	void SetPdgCodeLeading(Int_t pdgcode) { fPdgCodeLeading = pdgcode; }
	void SetPdgCodeSubleading(Int_t pdgcode) { fPdgCodeSubleading = pdgcode; }

private:
	enum Mode_t {
		kRadius,
		kEtaPhi
	};
	TVector3 GenerateParticle(const TVector3 *ref);

	Int_t					fPdgCodeLeading;
	Int_t					fPdgCodeSubleading;

	Mode_t					fMode;
	Double_t 				fFractionSubleadingPt[2];
	Double_t 				fEtaRange[2];
	Double_t				fDeltaR[2];					// delta r distance
	Double_t				fDeltaEtaRange[2];
	Double_t 				fDeltaPhiRange[2];

	ClassDef(AliGenPair, 1);
};

#endif /* ALIGENPAIR_H_ */
