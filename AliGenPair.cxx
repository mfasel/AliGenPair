/*
 * AliGenPair.cxx
 *
 *  Created on: Feb 3, 2016
 *      Author: markus
 */

#include "TVector3.h"
#include "AliGenPair.h"
#include "AliLog.h"

ClassImp(AliGenPair)

AliGenPair::AliGenPair():
	AliGenerator(),
	fPdgCodeLeading(0),
	fPdgCodeSubleading(0),
	fMode(kRadius)
{
	fEtaRange[0] = -0.8;
	fEtaRange[1] = 0.8;
	fDeltaR[0] = 0;
	fDeltaR[1] = 0.4;
	fDeltaEtaRange[0] = fDeltaEtaRange[1] = 0;
	fDeltaPhiRange[0] = fDeltaPhiRange[1] = 0;
	fFractionSubleadingPt[0] = fFractionSubleadingPt[1] = -1.;
}

AliGenPair::~AliGenPair() {
}

void AliGenPair::Init(){
	if(fPdgCodeLeading == 0 || fPdgCodeSubleading == 0)
		Fatal("Init","Please provide a non-zero pdg code");
	Info("Init", "Generating particle pairs of type %d|%d in eta[%.2f|%.2f] with %.2f < r < %.2f", fPdgCodeLeading, fPdgCodeSubleading, fEtaRange[0], fEtaRange[1], fDeltaR[0], fDeltaR[1]);
}

void AliGenPair::Generate(){
	TVector3 leadingpart = GenerateParticle(NULL),
			subleadingpart = GenerateParticle(&leadingpart);

	AliDebug(1, Form("leading particle has pt %f\n", leadingpart.Pt()));
	AliDebug(1, Form("sub-leading particle has pt %f\n", subleadingpart.Pt()));

	Float_t pleading[3], psubleading[3];
	leadingpart.GetXYZ(pleading);
	subleadingpart.GetXYZ(psubleading);

	Float_t polar[3]= {0,0,0};
	Float_t origin[3];
	Float_t time;
	Int_t i, j, nt;
	for (j=0;j<3;j++) origin[j]=fOrigin[j];
	time = fTimeOrigin;
	if(fVertexSmear==kPerEvent) {
		Vertex();
		for (j=0;j<3;j++) origin[j]=fVertex[j];
		time = fTime;
	}
	PushTrack(fTrackIt,-1,fPdgCodeLeading,pleading,origin,polar,time,kPPrimary,nt, 1., 1);
	PushTrack(fTrackIt,-1,fPdgCodeSubleading,psubleading,origin,polar,time,kPPrimary,nt, 1., 1);
}

TVector3 AliGenPair::GenerateParticle(const TVector3 *ref){
	Double_t pt, eta, phi;
	if(!ref){
		// Leading particle
		pt = fRandom->Uniform(fPtMin, fPtMax);
		eta = fRandom->Uniform(fEtaRange[0], fEtaRange[1]);
		phi = fRandom->Uniform(fPhiMin, fPhiMax);
	} else {
		// Subleading particle in deltaR distance from the track
		double ptmin, ptmax;
		if(fFractionSubleadingPt[0] >= 0) {
			ptmin = fFractionSubleadingPt[0] * ref->Pt();
			ptmax = fFractionSubleadingPt[1] * ref->Pt();
		} else {
			ptmin = fPtMin;
			ptmax = ref->Pt();
		}
		pt = fRandom->Uniform(ptmin, ptmax);
		bool found = false;
		Double_t etasign = fRandom->Uniform(0, 1) > 0.5 ? 1. : -1.,
				phisign = fRandom->Uniform(0., 1.) > 0.5 ? 1. : -1.;
		Double_t deltaEta = 0, deltaPhi= 0, deltaR = fDeltaR[1] + 1;
		if(fMode == kRadius){
			// simulate radial distance
			while(deltaR > fDeltaR[1] || deltaR < fDeltaR[0]){
				deltaEta = this->fRandom->Uniform(fDeltaR[0], fDeltaR[1]);
				deltaPhi = this->fRandom->Uniform(fDeltaR[0], fDeltaR[1]);
				deltaR = TMath::Sqrt(deltaEta*deltaEta + deltaPhi*deltaPhi);
			}
		} else {
			// simulate distance in both directions separately
			deltaEta = (TMath::Abs(fDeltaEtaRange[1] - fDeltaEtaRange[0]) < 1e-5) ? fDeltaEtaRange[0] : fRandom->Uniform(fDeltaEtaRange[0], fDeltaEtaRange[1]);
			deltaPhi = (TMath::Abs(fDeltaPhiRange[1] - fDeltaPhiRange[0]) < 1e-5) ? fDeltaPhiRange[0] : fRandom->Uniform(fDeltaPhiRange[0], fDeltaPhiRange[1]);
		}
		eta = ref->Eta() + etasign * deltaEta;
		phi = TVector2::Phi_0_2pi(ref->Phi()) + phisign * deltaPhi;
	}
	TVector3 result;
	result.SetPtEtaPhi(pt, eta, phi);
	return result;
}
